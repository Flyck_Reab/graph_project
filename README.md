# Graph_Project

# Project

- Implement a student admission program using the table marriage algorithm
- Teams of 2
- Input :
	- Student and school preferences from a file in the format used in TD
	- User selects who does the bidding
- Output :
	- Student to school assignment
	- Number of rounds needed to converge
- Evaluation :
	- Demo during last session, June 3
	- Report + code by June 13
- The report includes :
	- User manual
	- The creation process
# Exemples
On these exemples :
- "A", "B" and "C" are considered as students
- "Alpha", "Beta", "Gamma" are considered as schools
## Exemple 1
### Data :
| |A|B|C|
|---|---|---|---|
|Alpha|2, 1|1, 2|3, 1|
|Beta|1, 2|2, 1|3, 2|
|Gamma|3, 3|1, 3|2, 3|

### Result :
|Alpha|Beta|Gamma|
|--------|-------|--------|
|__A__, ~~C~~|B| |
|A|__B__, ~~C~~| |
|A|B|C|

|A|B|C|
|---|---|---|
|Beta|__Alpha__, ~~Gamma~~||
|Beta|Alpha|Gamma|

## Exemple 2
### Data :
| |A|B|C|
|---|---|---|---|
|Alpha|1, 2|2, 3|3, 1|
|Beta|2, 1|3, 1|1, 2|
|Gamma|3, 3|2, 2|1, 3|

### Result :
|Alpha|Beta|Gamma|
|-----|----|-----|
|__A__,~~C~~|B| |
|A|__B__, ~~C~~| |
|A|B|C|

|A|B|C|
|---|---|---|
|Alpha| |__Beta__, ~~Gamma~~|
|Alpha|Gamma|Beta|