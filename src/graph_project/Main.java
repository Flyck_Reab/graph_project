package graph_project;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import graph_project.binaries.Data;
import graph_project.binaries.Element;

public class Main {

	public static final String PATH_DATA = "data/data.json";

	public static void main(String[] args) {
		//Say hello because we're nice
		System.out.println("Hello world !");

		//Load data
		Data data = Data.loadDataFromJson();

		//Asks who is bidding, then start it
		if (isSchoolsBidding()) {
			matingRitual(data.getSchools(), data.getStudents());
		}else {
			matingRitual(data.getStudents(), data.getSchools());
		}
		
		//print result		
		printResult(data);
	}

	/**
	 * Ask to the user which side will do the bidding
	 * @return true if schools do the bidding, false if it is the students
	 */
	private static boolean isSchoolsBidding() {
		int choice;
		try (Scanner scanner = new Scanner(System.in)){
			do {
				System.out.println("Who will do the bidding ?");
				System.out.println("    1) Schools");
				System.out.println("    2) Students");
				try {
					choice = scanner.nextInt();
				}catch (InputMismatchException e) {
					return isSchoolsBidding();
				}
			} while (choice != 1 && choice != 2);
		}
		return choice == 1;
	}

	/**
	 * Process of bidding schools and students
	 * @param biddingElements will do the bidding, therefore they are be preferred by the algorithm
	 * @param biddedElements will be the list in which the bidding choose their mate
	 */
	private static void matingRitual(List<Element> biddingElements, List<Element> biddedElements) {	

		//counter for then number of steps to converge
		int numberOfSteps = 0;
		
		do{
			numberOfSteps ++;
			
			System.out.println("\n=================================================");
			System.out.println("================ BIDDING STEP " + numberOfSteps + " =================");
			System.out.println("=================================================");
			
			//Each bidding element has to bid while bidding is not complete
			for (Element biddingElement : biddingElements) {
				//If the element can bid with more than one element and have not bided everyone yet, do so
				while (biddingElement.getBiddedElements().size() < biddingElement.getCapacity() && biddingElement.hasNextPreference()) {
					//Obtain the next element to bid
					Element biddedElement = biddingElement.getNextPreference();
					System.out.println(biddingElement.getName() + " will bid the next element : " + biddedElement.getName());
					//Bid the element 
					biddingElement.addBiddedElement(biddedElement);
					//Inform the element that it is bidded
					biddedElement.addBiddedElement(biddingElement);
				}
				//If the capacity is not full but has no one to bid to, say it
				if (biddingElement.getBiddedElements().size() < biddingElement.getCapacity() && !biddingElement.hasNextPreference()) {
					System.out.println(biddingElement.getName() + " can't bid anymore");
				}
			}
			
			//print the information after bidding
			System.out.println("\n============= BIDDING OF THE STEP ===============");
			printElementsAndBiddings(biddingElements, biddedElements);
			
		}while(! IsMatingEnded(biddingElements, biddedElements, numberOfSteps)) ;

		System.out.println("\n\n");
		System.out.println("=================================================");
		System.out.println("=============== BIDDING COMPLETE ================");
		System.out.println("=================================================");
		System.out.println("Number of steps : "+ numberOfSteps);
	}

	/**
	 * Removes excessing elements from the bidded list, if such element exists.
	 * Then indicates if elements has been removed.
	 * @param biddingElements the bidding elements
	 * @param biddedElements the bidded elements from which the excess is removed
	 * @param numberOfSteps the bidding step number
	 * @return whether the bidding has changed
	 */
	private static boolean IsMatingEnded(List<Element> biddingElements, List<Element> biddedElements, int numberOfSteps) {
		boolean matingEnd = true;
		
		//Check if excess elements exists
		for (Element biddedElement : biddedElements) {
			if (biddedElement.getBiddedElements().size() > biddedElement.getCapacity()) {
				biddedElement.removeLeastPrefered();
				matingEnd = false;
			}
		}
		
		//if mating is not ended, print the new values
		if (!matingEnd) {			
			System.out.println("\n=== AFTER REMOVING EXCESS BIDDING ELEMENTS ====");
			printElementsAndBiddings(biddingElements, biddedElements);
		}

		return matingEnd;
	}

	/**
	 * Print a the list of students and schools bidding
	 */
	private static void printElementsAndBiddings(List<Element> biddingElements, List<Element> biddedElements) {
		System.out.println("Bidding elements :");
		for (Element biddingElement : biddingElements) {
			System.out.println(biddingElement);
		}

		System.out.println("\nBidded elements :");
		for (Element biddedElement : biddedElements) {
			System.out.println(biddedElement);
		}
	}

	/**
	 * Print the final bidding
	 */
	private static void printResult(Data data) {
		//Indicate whether all schools have at least one student
		if (Element.isAllElementsBidded(data.getSchools())) {
			System.out.println("All schools have at least one student");
		}else {
			System.out.println("/!\\ Some school does not have at least one student /!\\");
		}

		//Indicate whether all students have at least one student
		if (Element.isAllElementsBidded(data.getStudents())) {
			System.out.println("All students have a school");
		}else {
			System.out.println("/!\\ Some student does not have school /!\\");
		}

		//print final bidding
		System.out.println();
		System.out.println("\nFinal bidding");

		System.out.println("\nSchools :");
		for (Element biddingElement : data.getSchools()) {
			System.out.println(biddingElement);
		}
		System.out.println("\nStudents :");
		for (Element biddedElement : data.getStudents()) {
			System.out.println(biddedElement);
		}
	}
}