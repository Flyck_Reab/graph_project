package graph_project.binaries;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class School extends Element {
	
	public School() {
		
	}

	@Override
	public void init(Data data){
		this.preferences = new HashMap<>();
		this.biddedElements = new LinkedList<>();
		this.preferenceIndex = 0;
		for (List<Integer> studentIndex : data.getData().get(data.getSchools().indexOf(this))) {
			Integer preference = studentIndex.get(0);
			Element student = data.getStudents().get(preferences.size());
			if (preferences.containsKey(preference)) {
				System.err.println("The school "+ this.name +" has more that one student for the preference level "+ preference);
				System.exit(3);
			}
			preferences.put(preference, student);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		School other = (School) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String biddedStudentString = "";
		if (biddedElements!=null)
		{
			for (Element element : biddedElements) {
				biddedStudentString += " " + element.getName();
			} 
		}
		return "School [name=" + name + ", capacity=" + capacity + ", biddedStudent=" + biddedStudentString + "]";
	}
}
