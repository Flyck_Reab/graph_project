package graph_project.binaries;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Student extends Element{
	
	public Student() {
		//a student is initalized with a capacity of 1
		this.capacity = 1;
	}

	@Override
	public void init(Data data){
		this.preferences = new HashMap<>();
		this.biddedElements = new LinkedList<>();
		this.preferenceIndex = 0;
		for (List<List<Integer>> schoolIndex : data.getData()) {
			int preference = schoolIndex.get(data.getStudents().indexOf(this)).get(1);
			Element school = data.getSchools().get(preferences.size());
			if (preferences.containsKey(preference)) {
				System.err.println("The student "+ this.name +" has more that one school for the preference level "+ preference);
				System.exit(1);
			}
			preferences.put(preference, school);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String biddedSchoolString = "";
		if (biddedElements!=null)
		{
			for (Element element : biddedElements) {
				biddedSchoolString += element.getName() + " ";
			}
		}
		return "Student [name=" + name + ", biddedSchools=" + biddedSchoolString +"]";
	}

}
