package graph_project.binaries;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import graph_project.Main;

public class Data {
	
	/**
	 * The list of schools, loaded from Json
	 */
	private List<Element> schools;

	/**
	 * The list of students, loaded from Json
	 */
	private List<Element> students;

	/**
	 * The double entry list of correspondence between schools and students
	 */
	private List<List<List<Integer>>> data;

	
	/**
	 * Load schools from json
	 * Load students from json
	 * Set correspondence to schools and students
	 * @return the completed data
	 */
	public static Data loadDataFromJson() {
		//Extracts schools and students names / capacities from data.json file
		Data data = extractJson();
		//Load schools and student correspondences
		try {
			for (Element school : data.getSchools()) {
				school.init(data);
			}
			for (Element student : data.getStudents()) {
				student.init(data);
			}
		}catch(Exception e) {
			//If failed, then there is an error in the Json 
			System.err.println("Json format invalid");
			System.exit(2);
		}
		return data;
	}

	/**
	 * Put automatically the json data in this the values.
	 * @return a Data object knowing schools, students, and correspondence. But schools and students, don't know their correspondance.
	 */
	private static Data extractJson() {
		Data data = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			data = mapper.readValue(Main.class.getClassLoader().getResourceAsStream(Main.PATH_DATA), Data.class);
		} catch (IOException e) {
			System.err.println("Unable to read the json file");
			System.exit(1);
		}
		return data;
	}
	
	public List<Element> getSchools() {
		return schools;
	}
	
	public void setSchools(List<School> schools) {
		List<Element> elements = new LinkedList<>();
		elements.addAll(schools);
		this.schools = elements;
	}
	
	public List<Element> getStudents() {
		return students;
	}
	
	public void setStudents(List<Student> students) {
		List<Element> elements = new LinkedList<>();
		elements.addAll(students);
		this.students = elements;
	}
	
	public List<List<List<Integer>>> getData() {
		return data;
	}
	
	public void setData(List<List<List<Integer>>> data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "Data [schools=" + schools + ", students=" + students + ", data=" + data + "]";
	}
}
