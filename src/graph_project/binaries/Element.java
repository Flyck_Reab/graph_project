package graph_project.binaries;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class Element {

	/**
	 * The name of the element
	 */
	protected String name;
	
	/**
	 * The maximum number of bidding possible
	 */
	protected int capacity;

	/**
	 * The correspondence between elements and their preference order
	 */
	protected Map<Integer, Element> preferences;
	
	/**
	 * List of elements which are bidden by this element
	 */
	protected List<Element> biddedElements;

	/**
	 * An index to iterate over the preference list
	 */
	protected int preferenceIndex;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public List<Element> getBiddedElements() {
		return biddedElements;
	}

	public void addBiddedElement(Element biddedElement) {
		this.biddedElements.add(biddedElement);
	}

	public void removeBiddedElement(Element biddedElement) {
		this.biddedElements.remove(biddedElement);
	}

	public Map<Integer, Element> getPreferences() {
		return preferences;
	}
	
	/**
	 * @return the next element prefered
	 * If !hasNextPreference() then return the first element in the order of preference
	 */
	public Element getNextPreference() {
		if (this.preferenceIndex >= Collections.max(this.preferences.keySet())) {
			preferenceIndex = 0;
		}
		do {
			this.preferenceIndex ++;
		}while(this.preferences.get(preferenceIndex) == null && hasNextPreference());
		return this.preferences.get(preferenceIndex);
	}

	/**
	 * @return true if the index is lower than the maximum preference value found
	 */
	public boolean hasNextPreference() {
		return this.preferenceIndex < Collections.max(this.preferences.keySet());
	}

	/**
	 * while the element bid more elements than it's capacity
	 * removes the least preferred bidded element
	 */
	public void removeLeastPrefered() {
		int index = Collections.max(this.preferences.keySet());
		while (biddedElements.size() > this.capacity) {
			Element leastPreferedelement = this.preferences.get(index);
			if (leastPreferedelement != null) {
				leastPreferedelement.removeBiddedElement(this);
				this.biddedElements.remove(leastPreferedelement);				
			}
			index--;
		}
	}
	
	public static boolean isAllElementsBidded(List<Element> elements) {
		boolean isElementsBidded = true;
		for (Element element : elements) {
			if (element.getBiddedElements().size() <= 0) {
				isElementsBidded = false;
			}
		}
		return isElementsBidded;
	}

	/**
	 * Complements the preference list from the given Data
	 * @param data the data containing the schools, students, and preferences list.
	 */
	public abstract void init(Data data);

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract String toString();
}
